The compiler will consist of a process pipeline of components, each of which
handles a specific part of the compilation. Hooks will be exposed at each step
for compiler extensions to add to what the compiler can do. Additionally, an
API to get at the AST will be provided to make IDE integration simpler.

The end result is a distribution package which includes:

    * a platform independent funq blob,
      which may also contain files from the project's resources directory
    * a platform-specific binary that bootstraps the application
      (essentially a QApplication based binary that includes the VM runtime)?
    * metadata, dependency, data and cache files

The compiler may require a platform hint to know which files to include or how
to build included native code.

= Compiler hooks

* pre- and post-function additions

  Use case: test framework instruments functions to know which are run
            for which test

* arbitrary post-parsing processing

  Use case: documentation framework can compare documentation with actual
            current function signatures and returns

* token registration

  Use case: test framework will add new function tags and syntax which it will
            need to translate either into funq or bytecode

* distribution binary creation

  Use case: creating a texture atlas from images in resources/

= AST API

* traversal of tokens (cpt. obvious :)
* autocompletion
* module and function usage reports
* refactoring aids
    * if/else massage (reverse, invert, ..)
    * function and variable renaming
    * function parameter changes (i.e. order)
    * ...
* documentation stub generation
