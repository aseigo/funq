= fun(q): Functional Qt

funq aims to be a pragmatic, mostly-functional language for use with Qt that
brings the concept of "green processes"[1] to the platform. The goal is to
increase the reliability and robustness of Qt applications, make writing
concurrent code easy and safe, while opening the door to dynamic distributed
applications that can run across multiple devices.

Key attributes of funq will be:

    * Simple, celar syntax that is familiar to those used to Qt, QML and/or C++
    * Integrated Qt with QAbstractItemModel and QObject bridges
    * Designed to be used with QML for its GUI
    * Lightweight "green" processes with per-CPU-thread schedulers
    * Powerful message queues for communicating and coordinating between
      green processes in the same VM or across VMs

QML brought declarative syntax and a radically different approach to user
interface construction to Qt by following a similar pattern of familiarity,
integration and clear benefit. funq is intended to do the same for the business
logic in applications, particularly when parallelization and/or
compartmentalization would be beneficial.

Why not simply bind an existing functional language? Not only is the syntax
of nearly every mature functional language quite foreign to those more used to
the Algol family of languages (e.g. C/C++), but any binding between them and Qt
would run into significant barriers and limitations. Any awkwardness in the
bridging between Qt and the target language would hinder adoption.

Most functional languages also lack an implementation of green processes, which
is a primary motivator in this entire exercise. Even C/C++ can not be
retrofitted with this functionality as it requires running code in a virtual
machine under control of the application's native process. Coroutines and similar
features, yes; green processes, no.

Therefore, while it will likely be a larger effort than producing a simple
binding or adding something like, but not really, green processes to C++/Qt,
funq will be worth the effort due to the unique benefits it can bring.

[1] http://aseigo.blogspot.ch/2014/07/green-processes-multiple-process.htm
