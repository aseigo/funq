= Welcome :)

So, you are interested in getting involved with funq? That's terrific! We can
always use more hands on deck.

As you get started, please take a moment to appreciate and respect the culture
of the funq project. We are engaged in an exploration of topics that push at the
edges of the current state-of-the-art in free software when it comes to software
development tools and concepts. That is an exciting adventure to be on, and one
that asks each of us to be results-focused, team-oriented and to approach the
challenges at hands with the eagerness and curiosity of the explorer, the
scientist and the craftsman.

= Coordination and communication tools

We rely on tools to keep up the project moving and coordinated. Here are the
most important of them:

== Mailing list

Address: funq-devel@kde.org
Subscription & archives: https://mail.kde.org/mailman/listinfo/funq-devel

== IRC

Network: irc.freenode.net
Channel: #funq

== Patch review

All non-trivial patches as well as patches that a team member feels would
benefit from review must go through review on:

    https://reviewboard.kde.org/

Using the funq group as reviewers.

== Task list

We use a kanban board to track the open tasks. Tasks may be added and claimed
by any participant.

URL: https://todo.kde.org/?controller=board&action=show&project_id=18

== Defect reports

URL: https://bugs.kde.org
Component: funq

= Coding Standards

The funq code base follows the "KDE Library Coding Style" as found here:

    https://techbase.kde.org/Policies/Kdelibs_Coding_Style

Testing is a core principle of funq. Regressions in releases are not acceptable
and knowing the functionality and limitations of new code is critical to future
development and maintenance. Therefore all new code must be extensively unit
tested and all bug fixes must be accompanied by unit tests which test for the
defect to prevent future regressions.

Documentation is also a requirement. Without documentation it is not possible
for developers to use funq to its fullest. All functionality must be fully
and accurately documented in the docs, and any changes to functionality must
be accompanied by a change to documentation reflecting this change.

= Team & Community

The funq development team is friendly, open and strives for technical
excellence. We believe that no ideas are sacred and that the achievements of the
team is shared by every participant. The measure of achievement is defined
by the technical state of the product and its use and adoption.

Every component has at least one maintainer who is personally responsible for
that code. That includes ensuring quality, testing and documentation are up to
the project's standards, that reviews of patches are handled in a timely manner,
and questions regarding the code are resolved rather than left to languish in
never-ending discussion. New modules may not be added without a maintainer
commitment and maintainers are expected to find a  replacement maintainer when
they leave the team.

The team structure is "flat" in that any participant is welcome to work on any
part of the code base, from design to implementation, and all discussion and
decision making is open to everyone. Decisions are arrived at through consensus
found through technical discussion. Decisions taken by the team are considered
final and not up for re-discussion unless new information can be brought to the
table. In cases where consensus can not be found or there are multiple choices
deemed equally good, the maintainer(s) of the component in question will make
a final determination.

Additionally, the funq project embraces two foundational documents from the KDE
community:

    http://manifesto.kde.org/
    http://www.kde.org/code-of-conduct/


