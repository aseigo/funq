= Functionality that backends onto Qt

Networking
Database access
XML

= Qt Bridges

QAbstractItemModel
QObject

= Breaking the VM barrier: communication with the host app

IPC from processes in the VM to the C++/Qt code and vice versa

= Why Qt?

So, the elephant in the room: why Qt?

A primary motivation for creating funq has been to improve how end-user
applications are created. Supporting an existing application development
toolkit is not only less work, it grants access to an existing audience
of application developers.

Qt has emerged as _the_ free software application development toolkit. It
dominates in some industries and is to be found in just about every market
where software is made and consumed. This makes it a natural target.

Qt also provides an advanced and modern UI framework centered around a domain
specific language (DSL): QML. As part of the QtQuick offering, this has
received wide adoption, despite it not being imperative but rather declarative.
The Qt audience has already shown itself open to non-traditional development
paradigms as long as they are easy to use and present clear benefit.

QML, however, is not suited to being the sole application development language.
It begs to be paired with a robust application-focused DSL that can provide
benefits for application development as compelling as QML does for UI creation.

One thing the QML developers discovered was that pairing Qt code with a foreign
runtime (in their case Javascript engines from the Webkit project) resulted in
poor performance and real limitations in what was possible. Being able to
directly bring data and objects in their "native" Qt forms into the runtime
along with being able to pare down that runtime to just the feature set
needed was attractive enough for the QML team to design and write a new
Javascript runtime specifically for QML.

Learning from that multi-year exercise, it is clear that to bring Qt together
with funq requires tight integration to reach its full potential and the tight
focus will allow funq to remain simple enough to pick up quickly without the
baggage of extensive feature sets that are not relevant to developing graphical,
end-user applications

Finally, Qt is well supported across multiple platforms and is extremely
feature rich. Networking, database access, threading, XML, JSON, plugin and
library loading, printing, ... the list is extensive. All of these components
are cross platform and most are feature complete. By using Qt behind the
scenes, funq can target the same set of platforms Qt does while offering
a rich set of functionality in the standard library without having to do the
hard work of the system level implementations.
