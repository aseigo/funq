TODO

= std

std.i18n

== definitions

== types

These definitions are available for comparison with the return of functions
such as std.type.

(Implementation detail: They are defined as integer values.)

std.types.int
std.types.int64
std.types.float
std.types.double
std.types.string
std.types.binary
std.types.list
std.types.map
std.types.boolean

== functions

* tail(list)
* head(list)
std.types type(variable)

= std.process

= std.log

= std.json

= std.network

= std.dbus

= std.xml

= std.list

= std.error

== functions

code
category
message
pid
createError

= std.string

a QString + QRegExp bridge
