= Function overview

The essential form of a function is:

    [function tags] functionName([parameter list]) [when <preconditions>]
    {
        ... body ...
    }

Each of these parts will be covered in more detail below, but here is a quick
overview of what each part does:

Function tags provide hints to the compiler and allow more useful module
introspection.

The function name is used, as with most languages, to call the function. It is
also used to group multiple versions of a function into one larger virtual
function.

The parameter list contains all the variables that can be passed in. Not only
are they able to be defined by name, but also by pattern. (More on this below.)

Preconditions are an optional boolean expression that define tests the passed
parameters must match for the function body to be run.

Collectively, the function name, parameter list and preconditions are called the
function header. The header is used to differentiate between different versions
of a function. More on this in a bit.

Finally, the body is the block of code associated with this version of the
function.

= Function tags

Functions can be tagged with a variety of terms that tell the system what this
function is for. When tagged, a function may be checked at compile time for
having the right number of parameters and similar requirements.

Tags also marks the function for approved usage in certain contexts. For
instance any function meant to be used in a precondition clause should be tagged
with "precondition":

    precondition isEven(x) { x % 2 = 0 }

Finally, tags allow functions to be found in modules by introspection. All
functions tagged with "receiver" will be found by the std.process::addReceivers
function.

A function may be tagged with more than one tag, separated by spaces:

    statefull main runThis(State) { }

Common tags include:

    * receiver: this function is used to receive messages from the process
                message queue
    * main: this function can be used as the starting function in a process
            where a wait-loop on the message queue is desired
    * statefull: this function maintains state; to be used with the main tag
    * precondition: this function can be used in precondition clauses
    * public: this function is visible to other modules that import its module

= One function, multiple versions

One function may have multiple versions defined. Each version has its own body
and is differentiated by the parameter list and precondition clause. All
versions of a single function are grouped together as one virtual function in
the runtime.

For instance this defines a single function with three versions:

    fibonacci(1) { 0 }
    fibonacci(2) { 1 }
    fibonacci(int x) when x > 2 { fibonacci(x - 1) + fibonacci(x - 2)· }

Which version is run depends on the parameter passed, but to the rest of the
code it looks like one single function. The above would be equivalent to this
in C/C++:

    int fibonacci(int x)
    {
        if (x < 1) {
            return -1; // or perhaps throw an exception
        }

        if (x == 1) {
            return 0;
        } else if (x == 2) {
            return 1;
        }

        return fibonacci(x - 1) + fibonacci(x - 2);
    }

As you can see funq is more compact, easier to read and easier to extend.

The order function variants are declared in matters, as they are pattern
matched in that order. If more than one function variant matches a given set
of input parameters, the one that was declared first gets selected.

Calling a function with parameters that do not match any version of a function
results in a NO_MATCHING_FUNCTION error being returned. See the documentation
on error handling for more on this topic.

Different versions of a function must appear consecutively in the source code.
This is not legal, for instance:

    fibonacci(1) { 0 }
    fibonacci(2) { 1 }
    someOtherFunction() { true }
    fibonacci(int x) when x > 2 { fibonacci(x - 1) + fibonacci(x - 2)· }

= Parameter power

Parameter lists may contain variable names or literal values. When a literal
value is used, that function will only be called when the parameter passed in
matches that value. For instance this function:

    myFunc("blue") { "My favorite!" }
    myFunc("yellow") { "Not bad.." }
    myFunc(x) { "I don't know about that" }

Will result in different responses depending on whether "blue", "yellow" or some
other value is passed to it. You can free mix literals and variables in a param
list:

    myFunc(x, "yellow", y) { ... }

This function body will only be run when myFunc is called with three parameters,
the second of which is equal to the literal "yellow". The other two parameters
will be visible in the function body as variables named x and y.

Parameters may also define the type they expect. For example:

    fibonacci(int x) when x > 2 { fibonacci(x - 1) + fibonacci(x - 2)· }

only runs if the first parameter is an integer. Parameters without types
specified can be of any type, and parameter types can be different across
function variants. Example:

    add(int x, int y, z) { val f = x + y; std.debug::log(z); f; }
    add(string x, string y, string z) { val f = x + y; std.debug::log(z); f; }

This is called pattern matching, and there is a lot more than can be done with
it, such as matching the contents of lists and maps for easy filtering. See
the documentation on pattern matching for more information on this feature.

= Preconditions

The precondition clause is a boolean expression that is applied to the variable
parameters. This allows more sophisticated checks than just literal equality
while also retaining access to the variable in the function body. In the
earlier fibonacci example there was this version of the function:

    fibonacci(int x) when x > 2 { fibonacci(x - 1) + fibonacci(x - 2)· }

The precondition list checks that x is an integer and that it is greater than
2. Any boolean expression may be defined here and use of logical and (&&), or
(||) and not (!) operators along with grouping with parens '()' is supported.

The only rules for preconditions are:

    * it must evaluate to a boolean true/false
    * new variables may not be defined in the precondition clause
    * only parameters in this version of the function may be referenced

= Body

The body of a function may consist of any number of sequential lines of code.
This include defining new local functions and closures. See the documentation
on scoping for more information on how local functions and closures behave when
it comes to local variables and function parameters.

The last line of the body that is executed is also the return value of the
function. Every function has a return value. There is no such thing as a
function that has a void return as in C/C++.

This function:

    isEven(int x)
    {
        if (x % 2) { true }
        else { false }
    }

returns true if x is even and false if it is not.

Each line of code ends with a semi-colon and whitespace is generally ignored.
One exception is that the last line of a block does not require a semicolon.

= Local functions with the func operator

A function can be defined inside of another function using the "func" operator:

    myFunc(x)
    {
        val isEven = func(x) { x % 2 == 0 };
        if (isEven(x)) {
            "It is even!";
        } else {
            "Very odd indeed ...";
        }
    }

Such functions may be defined with pattern matching versions:

    myFunc(x)
    {
        val isEven = func(x) when x % 2 == 0 { "It is even" }
                         (42) { "The answer!" }
                         (x) { "Very odd indeed ..."};

        isEven(x);
    }

Such functions are only visible inside the function they are defined. However,
they may be the return value of a function:

    myFunc(x)
    {
        val isEven = func(x) { x % 2 == 0 };
        // perhaps some other code
        isEven;
    }

There is a shorter way to write that function, however ...

= Anonymous functions

Functions are not required to be assigned a name. These are known as anonymous
functions. So the previous example could be boiled down to:

    myFunc(x) { func(x) { x % 2 == 0 } }

This creates and returns a function that can be called any number of times
later on in the application to test if the passed in parameter was even or not.

Anonymous functions may also appear in the top level of modules. This is
generally only useful when used with tags for later introspection, such as in
this example:

    receiver func(PID, "countable", *, State)
    {
        State.messageCount++;
    }

Such a function is not able to be called directly from other code, but it will
be found via introspection when all message queue receivers are requested.

= Function chaining
TODO

Instead of nesting function calls, chain them such a way that the output of one becomes
the parameters for the next.
