= Variable typing

* explicit type can be prefixed to a variable or function parameter
    * more hints to compiler for potentially more efficient instructions
    * compile-time checking of usage
* without an explicit type, the type is infered (similar to C++'s auto)

= Numeric
TODO

* int being 32 bit
* int64 being 64 bit
* float
* double

= Strings

Strings in funq are unicode safe and data of copied strings is implicitly
shared until modified. Strings are denoted with single or double quotes; both
are equivalent. Single character strings are stored internally as unicode chars
for compact representation.

The std.string library provides standard string functions, localization and
regular expression facilities for string manipulation.

= Binaries
TODO

= Lists

Lists are a key data type for most functional languages, and funq is no
exception. In funq, lists are indexed series of mixed variables and as such are
the most commonly used generic data type.

A list is declared by enclosing a set of comma separated variables in square
brackets:

    var list myList = [ 1, 2, 3, 4, "string", 4.03, [ "inner", "list" ] ]

The std.list library provides a number of useful and common functions for
processing and transforming lists including folds, maps, zips and more.

= Maps
TODO
Comma separated, enclosed with curly braces {}, pairs in "key = value" form
May be updated

== Booleans
Booleans are simple primitives which have the value of either true or false.
Note that these values are case sensitive, so this works:

    val True = 1 != 0;

This, however, generates a syntax error on compile:

    val true = 1 != 0;

To explicitly declare a boolean, use the variable type bool:

    val bool truth = 1 != 0;

Unlike in C/C++, booleans are not equivalent to numeric values and comparing a
numeric value to a boolean will always result in false.

= Defines
To create a definition, simply assign a literal value to a variable outside of a function. In fact, assignment of literal values to a variable is the only kind
of variable declaration permitted outside of function bodies. Use of these
variables is replaced at compile time with the literal value for efficient
execution while avoiding the use of 'magic values' in your code. In other words,
they are functionally equivalent to #define in C/C++.

By default, such variables are not visible outside the module they are declared
in. To make them visible outside the module, the may be tagged as public.

Examples:

    MyModule
    {

    val MY_DEF = 100; // a private definition
    public val int OTHER_DEF = 200; // visible as MyModule::OTHER_DEF
    val THIRD_DEF = MY_DEF; // assigning a definition to a definition is fine
    val FOURTH = 3 + MY_DEF; // also fine as this evaluates to a literal value
    val FIFTH = myFunction(); // not a literal value; creates an error
    val sixth = "6"; // can be lower case, of course, and any literal type
    val FD = openFile("foo"); // error: not a literal value but a resource

    }

= Interface

An interface is a list of functions that combined define a named set of
functions that a module may implement. See the Interfaces section of the
Module documentation for more information. Other than the automatic
addition of the list to the module metadata and its allowance of being
instantiated outside of a function, it behaves as any other list.

= Tables
TODO
QAbstractItemModel bridge
