= Arithmetic

The arithmetic operators in funq follow the familiar patterns from C++ and
similar languages:

    Addition        a + b
    Subtraction    a - b
    Unary plus      +a
    Unary minus     -a
    Multiplication  a * b
    Division        a / b
    Modulo          a % b

= Assignment and Comparison

The assignment operator, '=', serves double duty: it does both assignment and
comparison. This allows one to assign, compare or combine the two operations
and do a conditional assignment pending successful comparison.

    val x = 3; // new variable x now equals 3
    x = 3; // correct, no error
    x = 4; // throws an error

    val pair = {2, 3};
    val (x, y) = pair; // x = 2, y = 3
    (val a, val b) = pair; // same as above, more verbose
    (x, y) = pair; // matches x to 2 and y to 3: success
    (val c, 3) = pair; // assigns 2 to c if the 2nd value in pair is 3: success
    (val d, 4) = pair; // assigns 2 to d if the 2nd value in pair is 4: error
    (val d, y) = pair; // assigns 2 to d if the 2nd value in pair equals y: yes!


When an assignment/comparison fails, an error is thrown which, if not caught,
will cause the process to crash. Function preconditions and if statements both
catch such errors to treat the result as a boolean. To perform the same
conversion yourself easily you can use std.convert:

    std.convert(bool, (val x, 3) = pair);

This is most useful for returning a boolean result from a function rather than
crashing the process (though the latter is usually what is wanted):

    func foo(y)
    {
        val pair = {2, 3};
        std.convert(bool, (val x, y) = pair);
    }

It assigns x on success (to no real point), and return true/false.

This function:

    func foo() { val x = 3 }

returns the result of the assignment which is 'x', which in this case is 3. So
this function:

    func foo(y) { val x = y }

is equivalent to this one:

    func foo(y) { y }

!= is a comparison operator that checks for non-equality and always returns a
boolean value as a result. Examples:

    var x = 3;
    x != 4; // correct, returns true
    x != 3; // incorrect, returns false

Unlike the assignment comparison operator =, != may not be used in conjunction
with variable assignment. For example this will generate an error when compiled:

    var x != 3;

= Logical

The logical operators are as follows:

    Not             !
    And             &&
    Or              ||
    Exclusive Or    ^^ // really? probably not

= Bitwise

TODO

= Ranges

'..' is the range operator. It accepts a start value on the left and a final
value on the right, e.g.:

    1..10

The start and final values must be of the same type, or the final value must
be convertable to the start type. A list populated with values of the same
type as the start value is returned. So the above example is equivalent to

    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

Filtering a range can be accomplished with comprehensions:

    [ 1..10 as (value) when value % 2 = 0 -> value ]

Internally this processes the values without creating a temporary list, making
it more efficient for generating large sparse lists.

Ranges are only supported for data types with which a stepping order and
interval are well defined. One can not generate a range of floats or doubles,
for instance.

= Order of operations

The order of operator precedence is as follows:

    Multiplication and division
    Addition and subtraction
    Logical operations
    Assignment / comparison

Operators of equal precedence are evaluated left-to-right. Operations grouped
by parentheses "()" are evaluated as a unit; nested parentheses are evaluated
from the inside out.