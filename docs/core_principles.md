== Goal

An easy to learn language[1] that sits on top of existing Qt code[2] that offers a safe and easy multi-process paradigm to Qt developers. This would provide an improved method for developing application logic while using QML for the UI and Qt/C++ for the "heavy lifting" underneath.

[1] something a C++ developer could pick up in an afternoon
[2] analogous to QML

== Primary Features

* functional language
    * looks C++-ish: curly braces, module declarations similar to class def
    * single assignment / immutable variables only
    * no state outside of functions
    * first class and higher order functions
* run in a register based VM
    * tail call optimization
* green processes as first class feature
    * multiple schedulers (one thread per, up to one scheduler per CPU core)
    * declarative supervision tree definition
    * VM-to-VM process migration
    * DECISION: receive functions -> should they only apply to top-level function/module of process?
                                     or should all receives anywhere in a process get access to they
                                     message queue?
* Qt integration
    * QAbstractItemModel bridge
    * QObject bridge
    * Easy to use with QML due to those bridges
* interfaces (functional-style mixins)
    * a set of functions (name/arity) required and possible defined in a module
    * a module may provide default implementations for non-required
    * a module will tend to provide additional functions to make using the
      interface easier
    * .. thus looks a lot like polymorphism and inheritance, but it isn't
* integrated unit testing
    * unit tests included in the code for workflow proximity
    * compiler will double as test manager
        * code coverage
        * run tests when compiling
        * combine the above two: modified functions will auto-trigger relevant tests
        * compile reports of test runs
* comprehensions
* pattern matching
* function preconditions 
* small standard library to ease common patterns
* DECISION: atoms (implicit global enums)?
    * con: makes distributed funq harder?
    * pro: tidy syntax and nifty for pattern matching

== Std Library Features
* implementations of common patterns
    * recursion (fold, map, zip, ..)
* set of common interfaces
    * finite state machine (FSM)
    * supervisor
    * process
* message passing
    * between processes in same vm
    * between processes in different vm's
    * between process in a vm and application implemented in C++ / other lang
* message queue processing
    * FIFO
    * filter-based
* networking, FS from Qt
* generic cooperative editing protocol
    * coordinate any edit-based system between multiple simultaneous users
    * text editor, drawing program, ...
