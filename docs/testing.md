= Integrated Testing

As funq is intended for use in developing the business logic of an application,
it is critical to have a good testing framework to use with it. funq comes with
such an integrated testing framework, allowing you to include tests with your
sources.

In devel builds the test framework annotates functions and when tests are run
this creates a mapping of tests to functinos touched stored in a cache file in
the project's build directory. Using this information, the test framework can
run tests that touch functions which have been altered making it far easier to
run the most relevant subset of tests during compilation.

= should functions

Test functions may be mixed with regular funq source code as part of modules
or may appear in their own dedicate modules. This is a decision left up to the
developer.

Functions which contain tests are tagged with "should", and such functions
may only contain tests. They may not contain general application code and they
may not be called by other functions; doing so will result in a compile error.
Should functions take no parameters and generated errors are treated as
test failures.

All should functions are visible to the test framework which can list, run and
generate reports using them.

= Given-When-Then

The funq test framework implements the given-when-then idiom that is well known
in the agile world. Each test is implemented using a given-when-then block where
the meaning of each block is as follow:

    (Given) some context
    (When) some action is carried out
    (Then) a particular set of observable consequences should obtain

Any statements outside of a given, when or then block in a should function
generate a compile error.

== Given

The given block is where value declarations to be used in the tests are made.
Each should function may have at most one given block. In the trivial case, a
given block may contain simple val assignments:

    given {
        val input = 1;
        val expected = "B";
    }

Each variable declared in the given block will also be in scope of the when
and then blocks as well.

The real utility of given becomes apparent in the data driven case. For data-
driven tests, the given has a parameter list that defines the variables that
will be set on each iteration. Additionally, a state block may be provided which
is a thin wrapper around a map. The state is visible in the given block and can
be used to keep track of where in the data set given currently "is".

The given block is then run repeatedly until it returns a set of data that does
not match its parameter list, usually just an empty list. The when and then
blocks are then executed once for each set of data returned by the given
block.

    state {
        data = [ [0, "A"], [1, "B"] ]
    }

    given (input, expected) {
        [ val tuple | state.data ] = state.data;
        tuple;
    }

The given parameter list follows the same rules as function parameter lists,
meaning that this also a perfectly valid header:

    state {
        data = [ [0, "A", 1], [1, "B", 2], [2, "C" 3] ]
    }

    given (int input, string output, 3) {
        [ val tuple | state.data ] = state.data;
        tuple;
    }

Dynamic data sets are supported, such as by querying a database for values,
by using the state map to store the queried data:

    state {
        queryResults = false
    }

    given (username, firstname, lastname) {
        if (!state.queryResults) {
            state.queryResults = // do db query here
        }

        std.database.nextRow(state.queryResults);
    }

== When

When blocks follow the given block and are where the actual test computation
takes place. There must be at least one when block in each should function. Each
when block is run over the data defined by the given block.

Any mix of statements may occur in a when block, and the values defined in the
given block are visible inthe when block. The when block may also define values
itself for later consumption in the then block. Example:

    when {
        val output = convertColumnToString(input);
    }

== Then

Then blocks follow when blocks. Each when block must have exactly one then
block that follows it. Then blocks may only contain truth statements using the
variables generated in the given and when blocks. The failure of any of these
truth statements results in the failure of the test.

Example:

    then {
        output = expected;
    }

== An example test module

An example of a full test function follows:

    using org.foo.Conversions as Conversions

    TestConversions
    {

    should correctlyConvertKgToLbs()
    {
        given {
            val cms = 1;
            val inches = 0.393700787401575;
        }

        when {
            val output = Conversions.length(cms, "cm", "in");
        }
        then {
            output = inches;
        }

        when {
            val output = Conversions.length(inches, "in", "cm");
        }
        then {
            output = cms;
        }
    }

    }

= Running tests

TODO: relies on funq cli; should include configuration options
