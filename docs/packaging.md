= funq Packages

funq provides a standardized file layout for projects that makes tooling easier
and simplifies distribution. There are two kinds of funq packages:

    * source: what the developer manages
    * distribution: the results of compiling a source package

= Source Packages

Source packages are where all the source code, application data and built-time
support files are kept. Each type of file is kept in its own subdirectory of the
package on disk. The layout of a funq source package is as follows:

    / => Project root
        metadata => File containing project metadata (name, licensing, etc)
        dependencies => The list of external dependencies, if any
        build/ => where build artifacts are stored
        cache/ => pre-populated caches
        config/ => funq configuration directives
        data/ => files that are packaged but not compiled in-binary
        distribution/ => where distribution packages are created
        icons/ => directory containing various versions of the launcher icon
        native/ => C/C++ code to be compiled
        resources/ => non-source code data that will be compiled in-binary
        source/ => directory containing funq source code
        supervision/ => directory containing supervision descriptions

A new source package may be generated using the funq tool:

    funq project create <project name>

== Product Metadata

    ** TODO format for the metadata file

    ** IDEA include licensing and authorship information here to allow
       references from documentation to it

== Dependencies

    ** TODO format for a dependencies description so that at build or install
       time (both?) dependencies can be fetched and included. Think npm (nodejs)
       or gem (ruby)

== Icons

This directory contains icons to be used for launcher entries in application
menus. The icon names reflect the resolution of the icon, e.g. 128x128.png.
Icon files in the top level of the icons/ directory are considered platform
neutral and should be provided in a number of standard sizes.

Platform-specific icons should appear in subdirectories named after the platform
they target.

    ** TODO: define what "standard sizes" mean
    ** TODO: how are platform directories named, exactly?

== funq Source Code

Under the src/ directory you may have any directory structure that makes sense
to you for your project. The only stipulation is that only funq source code
files should exist here.

If it is to be a stand-alone application (or a service that can be included in
other funq programs) then exactly one function should be marked with the main
function tag. Having more that one such function will result in an error.

== Supervision

Supervision definitions are found in the supervision/ directory. The build will
automatically process them and make them available to the rest of the project
at runtime.

== Native Source Code

It can be necessary at times to include native code with a funq application.
This is supported by putting each compile target into its own subdirectory in
the native/ subdirectory along with a CMakeLists.txt detailing its build. Each
build must produce a funq plugin. If native sources are included, a functioning
C/C++ compiler must be available on the system as funq does not provide this.

    ** TODO: define what a "funq plugin" is exactly
    ** TODO: cmake infrastructure? (e.g. automatically look for Qt5, etc.?)

== Data

The data directory contains all files that should be present at application
start to ensure correct execution. This may include images, configuration files,
data files, etc. Cache files should not appear in this directory; only files
that should not be deleted but may be replaced or changed (e.g. by the user or
a system integrator) should appear here.

The std.files module provides easy access to these files from distribution
packages.

== Resources

Resources are data files that should be included directly in the distribution
binary. This can allow faster access to the data as well as simpler deployment.
Since these files are built into the distribution binary they can not be easily
replaced at run-time.

The std.files module provides easy access to these files from distribution
packages.

 ** IDEA: allow resource files to be overridden at runtime by files with
 ** the same path under data/?

 ** IDEA: take image files and create an atlas from them?

== Cache

Many applications use cache files for image, network and other repeatedly used
data that can be expensive in terms of computation or time to generate. One
strategy is to deliver per-populated caches with the application to give a newly
installed version of the application a head-start with its caches. This can
allow an application to start quicker and produce a nicer run-time experience
from the very first execution when the contents of the cache(s) are known
before-hand.

All files in the cache directory are included in generated distribution
packages. If the caches are platform-specific, they should appear in a sub-
directory named after the platform they apply to (e.g. x86, armv7hf, etc.).

== Build

The most obvious use for the build directory is intermediate files generated by
the compiler as source code is turned into distribution-ready binaries. In the
case of native code being included, a subdirectory for each target platform is
created.

During compilation code processors may also create files in the build directory. 
Processors such as test frameworks, documentation generation and checking and
others may produce generated code, data such as testing code-coverage
information, etc. These files also end up in the build directory.

Removing files in the build directory is harmless: they will be generated again
on the next build as needed.

== Distribution

This is where distribution packages are stored once they are generated. If the
generated package is platform-specific, it will appear in a subdirectory whose
name is the target platform.

See the section on distribution packages below for more information.

== Miscellaneous Files

By default, funq adds a .gitignore file to the project tree which directs git
to ignore the contents of both the build and distribution directories.

= Distribution

Distribution packages are what are deployed to user systems. These include the
compiled funq binaries, native compilations, data and pre-populated caches.
These packages are generated by the build process when launched with a funq
source package and are stored as a single zip compressed file. Within the zip
archive the file system layout is:

    bin/ => funq binaries
    native/ => native libraries (one subdir per target)
    data/ => data files
    cache/ => pre-populated caches

== Binaries

== Platform Libraries

== Data

== Cache
