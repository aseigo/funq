= Modules Overview

In funq, every function belongs to a module. This helps prevent name
collisions and provides for a mechanism to define and use interfaces.
To use a module from another file, the import statement is used.
Finally, a module by implement one or more interfaces which are defined
sets of functions that form a contract which the module must then implement.

Modules and import directives are the only identifiers allowed in the top level
of a funq source code file. All functions, definitions, symbols, test blocks,
etc. must be contained in a module block. Modules may not be nested, though 
more than one module may appear in a single file.

Here is a table of analogous concepts across languages:

    C++             QML             funq
    namespaces      filenames       modules
    includes        imports         imports
    virtual methods                 interfaces

= Defining a module

A module is created like this:

    MyModule
    {
        // ... lots of functions go here
    }

Module names may consist of letters, numbers and dots ('.'). No other
characters are permitted. Anonymous modules are not supported.

All built in modules begin with "std.". No module may use this prefix.

A version number be optionally provided directly after the module name. Version
numbers must begin with a number and may contain only digits and dots ('.').
Dots separate the version into component parts. For instance 1.2.43 could
represent the major, minor and patch version of the module. There is no limit
to the number of numeric components may appear in version number.

    MyModule 1.2
    {
        // ... lots of functions go here
    }

Modules that are part of libraries are by default not visible to the outside
world. Only modules marked as public are visible.

    public MyModule 1.2
    {
        // ... lots of functions go here
    }

Modules in an application may also be marked as public to be visible to other
applications running in the same VM. See the documentation on Packaging for
more information on libraries and applications.

= Interfaces

A module may optionally implement one or more interfaces which are listed
directly after a colon that follows the module name and version and before
the opening brace of the module block. A compile error will occur If a module
does not provide implementations of all required functions in an interface it
declares itself to implement.

    org.myproject.MyModule 1.2 : std.interface::server, org.myProject.Iface
    {
    ... functions, tests, definitions, etc. ...
    }

To define an interface variable of type interface in your module. The name of the variable will become the name of the interface and it will be exported
as part of the metadata of the module.

= Imports

Modules may access other modules by using the import directive. Once imported,
the imported module's public functions and interfaces may be used. An import
statement consists of a module's full name, an optional version description and
an optional alias to be used for the module in the file.

Version descriptions are made up of one or more version numbers along with an
optional operator. The list of supported operators are:

    * '>', greater than
    * '>=', equal to or greater than
    * '<', less than
    * '<=', equal to or less than
    * '..', a range of version numbers (inclusive)

If a version number is declared with no operator then that exact version of the
module will be used. If no version is provided, then the version of the requested
module with the highest version in the runtime will be used.

Examples:

    import std.Network 0.1 as Network;
    import org.myproject.MyModule as MyModule;
    import org.kde.Archive >= 5.1 as Archive;
    import org.kde.Archive 5.1..5.3 as Archive;
    import org.kde.Archive < 5.3 as Archive;

If the requested import can not be satisfied at compile time, an error will be
generated. If this happens at runtime (for instance due to a missing funq
library), a runtime error will occur and the application will crash.

At runtime, if a module name conflicts with an already loaded module, a
warning will be logged (see the Debugging documentation for more on logging)
and the new module will simply not be loaded.

= Introspection
TODO
* listing functions in a module
* listing interfaces implemented by a module
* listing other modules used by a module (?)

