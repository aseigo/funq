= Errors

When an error is generated at runtime, it is placed in a standard data structure
known as an "error object". In reality, this is just a simple list with a well
defined order and type of elements:

    [ int code, string category, string message, PID, [ list codeSpot ] ]

codeSpot itself is a list of form:

    [ string module, string function, int line ]

This only contains valid data in non-release builds that have this information
in the binary.

To aoid having to remember how an error is constructed, use of std.error is
encouraged. The most useful functions are std.error.throw which creates an
error from inputs and the error accessors such as std.error.message.

While you can certianly pattern match and access list elements directly in an
error object, using std.error may make this easier and more convenient.

= Generating errors

When generated, errors cause the immediate return of a function. The error is
returned to the caller to be caught. If that function does not catch the error
it causes the immediate return of *that* function and so-on until a top-level
function in the current process is reached.

If no function catches an error, then the process crashes. This is completely
acceptable in most cases thanks to the availability of supervisors which will
detect such crashes and can restart processes when appropriate.

Due to this, funq processes are encouraged to only capture errors for which
there is a well-defined recovery from. If the response to an error is to return
a "non-value" or an error code in response, then the process usually should
instead simply be allowed to crash.

Other processes may watch for such crashes if they need to respond appropriately
and onFailure policies may also dictate a function that may perform clean up
duties.

= Catching errors

To catch an error include a try/catch block in your function:

    func myFucntion()
    {
        try {

        } catch ([code, "someErrorType", ...]) {

        } catch (message) where std.error.type(message) = "anotherType" {

        }
    }

Catch blocks are for all intents and purposes inner functions, complete with
the usual abilities of functions for pattern matching, which expect a single
parameter of type list following the standard error pattern and which are
called automatically only when an error is thrown in the executation of an
associated try block.