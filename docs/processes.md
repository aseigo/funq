= Concurrency and Robustness

Processes in funq are light-weight objects that make writing concurrent, robust
code easier. Each process has a function (any function!) as its "main" and once
running is scheduled alongside all other running processes in the VM. Processes
communicate with other via the built-in message queue which is the building
block for creating multi-process applications in funq.

On multi-core systems, processes may be run on different CPU cores allowing a
multi-process application to scale with hardware resources.

If a process experiences an error, it can be allowed to crash without
interfering with any other process that is running. The other processes that
care to may ask to be informed of such failures so they may respond
appropriately. This allows the developer to write code that is not
"defensive": checking for every possible error at every turn. One can simply
write code for success and if an error happens the process crashes and is
complete. Trying to figure out every possible error condition and then writing
accurate tests for them is time consuming and hard to make perfect. Using
processes can allow you to skip most, if not all, of such defensive programming
in practice.

Orchestrating and managing processes is generally done using supervisors, which
are a special kind of process. See the documentation on supervision to learn
more about this aspect of processes.

= Starting a process with exec

Creating a new process is as simple as passing a function and optional
parameters to the std.process::exec call. Any function may be used in a process.

The exec call returns a process ID or PID.  Every process is assigned a unique
PID as a tuple of [ VM_address, process_number ]. This PID can be used to send
messages to the process or send signals (such as "terminate") to it.

    ProcessID = std.process::exec(MyModule::MyFunc);
    Numbers = [1, 2, 3];
    PID2 = std.process::exec(std::map, [ Numbers ]);

= Stoping a process

A process may stop itself with the std.process::exit() function. This results in
an immediate "clean" exit. If a failure has occurred and the process should be
directed to crash, std.process::crash(Error) should be used.

Both of these functions also take an optional PID. In that case the process is
scheduled for stoppage and is asynchronous.

= Starting a function with a supervisor

Processes may also be launched using a supervisor. Rather than using exec() to
directly spawn the new process, predefined process definitions are used.

    Supervisor = std.supervisor::get(["CardTradingGame", "Networking"]);
    PID = std.supervisor::spawn(Supervisor, "sale");

Whether a new process is spawned or an existing process PID is returned depends
on the configuration of the supervision tree. See the supervision documentation
for more information on how to define and use supervisors.

= Naming processes

A PID may be associated with one or more names using std.namePid:

    std.process::namePid(PID, "Main Process");

This allows one to later fetch the PID for a process by name:

    val PID = std.process::pidOf("Main Process");

If the requested process does not exist, an invalid PID will be returned. A
process may be associated with multiple names.

All names are registered in a global hash so there is some overhead associated
with this. If there is alread a PID registered with that name, the label
function will fail with an error. When a named PID exits, the name is removed
from the global hash and pidOf will return an invalid PID.

Using supervisors is usually more convenient.

= Tracking when a process exits

In some cases you will want to track the lifecycle of the process. This can be
useful to know when the process is complete or has terminated unexpectedly.
A process can be linked to when exec is called by passing a PID (or list of PIDs)
the process should be linked to as the third parameter:

    val PID = std.process::exec(MyFunction, [], std.pid());
    val PID = std.process::exec(MyFunction, [], [ std.pid(), PID2 ]);

This form is guaranteed to have no race conditions. Even if the process exits
immediately after exec, the linked processes will be informed of it. Another
way of linking to random processes is to explicitly call link:

    std.process::link(PID, std.pid());
    std.process::link(PID, [ std.pid(), PID2 ]);

Note that the process may exit before the link call is successful.

Once linked, any functions in the current process that are signalReceiver
functions will be called when the process exits. signalReceiver functions take
two parameters: the PID of the process that exited and an error. The error
will be null if the process exited cleanly.

    signalReceiver myFunc(PID, error) { ... }

= Inter Process Communication

Every process has a message queue which other processes may add messages to.
Each message in the queue has a sender address, a "conversation" ID, a symbol
used as a tag, and then the actual data which can be of any type:

    [ PID, ConversationID, Tag, Data ]

Sending a message to a process is done with "->" operator. This line:

    [ "numbers", [1, 2, 3] ] -> PID;

would send a message tagged as "numbers" with the list of three numbers as
the data to the process with id PID. This will show up on the receiving side as:

    [ SenderPID, "numbers", [1, 2, 3] ]

Of course, if one does not care about the message ID the send can be
written simply as:

    [ "numbers", [1, 2, 3] ] -> PID;

All messages sent in this fashion are asynchronous and no response will be
generated.

== Conversations

A conversation is two or more messages that are passed between processes in
which each message follows as a direct result of the one before it. Think of
a conversation between two rational human beings for an analog.

Starting, or continuing, a conversation is almost identical to the simple
send message syntax seen above:

    ConversationId = [ "numbers", [1, 2, 3] ] <-> PID;

Note the use of the "<->" operator rather than the "->" operator and that it
returns a conversation ID. A timeout may also be defined:

    ConversationId = [ "numbers", [1, 2, 3] ] <-> PID timeout 500;


Since messages are asynchonous, a reply may occur after other messages are
received. There are two ways to handle this. The first is to simply let the
response be added to the message queue for later processing, which is what
the above example does. (See the next section for message processing with
receiver functions.)

Alternatively, a function to be run when the response is returned
may be provided in the send line:

    [ "numbers", [1, 2, 3] ] <-> PID then someFunction;

.. or with a timeout:

    [ "numbers", [1, 2, 3] ] <-> PID then someFunction timeout 500;

... or blocking:

    [ "numbers", [1, 2, 3] ] <-> PID wait then someFunction;

In all three examples above, the function "someFunction" will be called
to process the response. Consider this example of an infinite ping pong battle:

    pinger(PID)
    {
        receiver reping(PID, "pong", *)
        {
            pinger(PID);
        };

        [ "ping" ] <-> PID then reping;
    }

    main pong
    {
        receiver ponger(PID, "ping", *)
        {
            [ "pong" ] -> PID;
        }
    }

    pinger(std.process::exec(pong));

What is that "receiver" function tag that just made an appearance?

== Processing messages with receive functions

Messages are processed by receive functions in the order they are added as
handlers. The first receive function that matches a given message is passed
the message and process stops there unless the process enqueues the message
again:

    receive (PID, ConversationId, *, message)
    {
        .. some processing ...
        // respond
        response -> PID;
    }

Deciding which receive functions match a message is done using standard
pattern matching against the [ PID, Tag, Data ] message tuple. Some practical
examples follow.

The message tag is useful for message routing. For instance, a message may be
tagged with "auth" to mark that it is an authentication request:


    receiver SuccessAlways(PID, "auth", message)
    {
        [ success ] -> PID;
    }


Receiver functions can also pattern match on the PID:

    receiver local100Listener({ local, 100 }, *, message)
    {
        [ "success", message ] -> [ local , 100 ];
    }

A receiver may also match on the message contents itself, both in the parameter
list and the preconditions clause. See the pattern matching documentation to
learn all about what pattern matching is capable of, and the functions
documentation to learn about the preconditions clause.

== Adding receiver functions

Messages are filtered through receive functions which can be generated using
the receive function tag. Any receive functions in the current scope of the
process are automatically used in message processing. For example this function:

    main processMain()
    {
        receiver (PID, message)
        {
            //this is an anonymous receiver function!
            std.log(i18n("Received %1 from %2", message, PID));
        };
    }

will have, by default, one receiver function (which in this case is also
an anonymous function).

Additional receiver functions may be added to the receiver set with:

    std.process::addReceivers(MyModule::MyFunc);
    std.process::addReceivers([ MyModule::MyFunc, OtherModule::Funky])

To add all receivers in a given module:

    std.process::addReceivers(MyModule);

This is particularly useful as it allows adding new receiver functions to
the module without having to add that function by name to every function
used as a process main.

Messages are dequeued for processing with:

    std.process::nextMessage();

As a shortcut, a function can enter a message dequeing loop with;

    std.process::processMessages();

This will put the process into a loop that waits for messages and dequeues
them for processing as they arrive. Functions tagged with "main" will
automatically have a dequeue loop appended to them, which makes it as easy as:

    main mainFunc()
    {
        std.process::addReceivers(MyModule);
    }

By adding the "module" tag, which automatically pulls in all receivers from
the function's module, it gets even shorter:

    module main mainFunc() {}

A module may have more than one function tagged with "main".

== Process pipelines

There is a convenience method for creating processing pipelines made up of
series of processes which process input and pass it on to the next one. This is
similar to the familiar pipe model found in UNIX shells.

To create a pipeline:

    * create a list containing a mix of PIDs and/or functions
    * create an input message which will be sent to the first entry in this list
    * optionally create a state value
    * pass these to std.process::pipeline

For example:

    val processes = [ Document::read, Utils::wordCount, std.process::pid() ];
    val state = [ filepath, someOtherBitOfState ];
    val message = [ "readFile", filepath ];
    val pipePID = std.process::pipeline(message, elements, state);

This will pass the input message to the first processes in the pipeline. Its
response will be sent to the next processes and so on until all elements have
completed processing.

Passing a function as a processes will cause a new process to be created just
for this pipeline which runs that function. Passing a PID will cause the
message to be sent to the PID directly without starting a new process.

This process is orchestrated by a supervisor process created automatically by
std.process::pipeline. This supervisor starts and stops the processes as needed
and handles passing the messages on to the next process in the pipeline.

= Processes with state

It is often useful to carry some state with a process. This is achieved by
using the "statefull" function tag and passing an initial state to the main
function. Example:

    statefull main statefullMain(State)
    {
        receive (PID, tag, message, State)
        {
            std.debug::log(i18n("Received %1 from %2", message, PID));
            State;
        }
    }

Note that all receive functions used in a statefull process must take a fourth
parameter that is the state data and must return a state data. This returned
state data will become the new state of the process.

= Parallelization with implicit process creation

The simplest way to take advantage of processes is by using functions which are
parallelizable. An example would be the map function which takes a collection
and applies a function to each item in the collection to create a new collection:

    val numbers = [1, 2, 3, 4];
    val doubled = std::map(Numbers, fun(X) { X * 2 });

This could also be done with a simple list comprehension, of course:

    val doubled = { Numbers (X) -> X * 2 };

Both of these can be turned into parallel processes which divide up
the work between them. For the functions that support this feature, the syntax
is standardized: simply add the parallel symbol '||' to the function name:

    val doubled = std::map||(Numbers, fun(X) { X * 2});
    val doubled = {|| Numbers (X) -> X * 2 };

To control the number of processes to spawn, include a number in the parallel 
operator:

    val doubled = std::map|2|(Numbers, fun(X) { X * 2});
    val doubled {|3| Numbers (X) -> X * 2 };

Numbers less than 1 are ignored and fewer processes than requested may be created
if the process scheduler deems it more efficient.

Processes created in this fashion do not have a message queue, a process ID and
limited memory management. This makes them even lighter than normal processes
started with exec().
