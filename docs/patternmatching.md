= You already know pattern matching!

Pattern matching refers to the practice of being able to add definitions in
parameter lists to define what they apply to. Many languages support the
concept of declaring the types of parameters, so in C one might find:

    int doubled(int value) { return value * 2; }

The parameter "value" is defined as being of type int. Calling foo with a
different type will result in a compile error. For languages with function
overloading, one might see things like:

    int doubled(int value) { return value * 2; }
    int doubled(std::string value) { return value.length() * 2; }

Now when doubled is called, the version of the function called depends on
whether or or not an int or a string is passed as the first parameter. This is
a (very) basic form of pattern matching: the compiler matches the
correct function with a given call by matching the parameter list definition.

Pattern matching in languages such as funq simply takes this basic concept and
adds additional ways to express constraints and perform assignment.

= Filter values

Consider this function:

    process("auth", username) { ... }
    process("logout", username) { ... }

The function "process" is defined as taking two parameters and having two
versions of its body. Which version of process is executed when it is called
depends not just on the parameter list types, but also on matching the literal
value defined as the first parameter. Calling process with a string that
has the value "auth" as the first parameter will cause the first version above
to be executed; similarly passing "logout" as the first parameter will cause
the second version to be executed.

This style of pattern matching is often used in function parameter lists
to pre-filter messages from the process message queue or to define which
function to run in a finite state machine.

So parameter lists may contain not just variable names and types, but also
literal values which are then used as filters.

Parameter lists are used in many other places than functions, such as in
comprehensions and assignemnts. In those cases, a filter value may also be
a variable. For instance:

    val people = [ ["bicycle", "Jerry"], ["car", "Susan"], ["train", "Jay"] ];
    val filter = preferedMethodOfTransport();
    val matching = [ people as (filter, val name) -> name ];

The matching list would now contain only the names of people who are using the
prefered method of transport.

= Defining valid states

Pattern matching may be used in conjunction with assignment to define valid
states. For instance:

    func rewardBicyclists(list person) {
        ("bicycle", val name) = person;
        reward(name);
    }

    val people = [ ["bicycle", "Jerry"], ["car", "Susan"], ["train", "Jay"] ];
    val filter = preferedMethodOfTransport();
    val matching = [ people as (filter, val name) -> reward(person); name; ];

The function rewardBicyclists accepts a list but requires that it contains two
elements, the first of which is the string "bicycle". If this violated, then
an error will be generated. This is a powerful way of defining the valid
state(s) a function is willing to accept. It creates a contract that
automatically results in an error when violated. This is the opposite of
defensive coding in which one checks for any and all possible error conditions;
instead it allows the developer to write code that works in specific states
and terminates with an error in all others.

= Wildcards

When pattern matching, you may not always care about every element in the
parameter list. For those cases there is the wildcard symbol: *. For instance,
consider this extension of the reward example above:

    func reward(["bicycle", name])
    {
        reward(name);
    }

    func reward(["train", name])
    {
        reward(name);
    }

    func reward([*, name])
    {
        ignore(name);
    }

The last version of the reward function will match any value as the first
element of the list without binding it to a variable.

= Limitations

There are limitations to the expressive power of pattern matching, and usually
when you run into such situations precondition clauses can be used to fill the
gap.