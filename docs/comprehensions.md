= Comprehensions

A comprehension is a way to define a transformation that should be applied to
the elements of a data set to produce a new data set. A list comprehension, for
example, defines an operation to be performed on each element of the list, one
by one; that operation returns a new element that is added to a new list, and
that new list is the result of the comprehension.

Comprehensions are available for lists, strings and binaries. Each are described
below.

= List Comprehensions

List comprehensions are enclosed in []s, as it is a list operation that creates
another list. The basic syntax is as follows:

    [ variable as (parameter list) when preconditons -> operation ]

The precondition is optional, but the rest is mandatory. A concrete example:

    val integers = 1..10;
    [ integers as (val integer) -> fibonacci(integer) ];

This takes the list of integers 1 through 10 and iterates through it. On each
iteration, the current element is known as "integer" and the operation defined
is to call the fibonacci function with that integer as its argument. This
creates a new list:

    [ 0, 1, 1, 2, 3, 5, 8, 13, 21, 34 ]

Preconditions can be used to filter the list before the operation is performed:

    [ integers as (val integer) when integer %2 = 0 -> fibonacci(integer) ];

The paraemeter list supports the usual pattern matching facilities. So for lists
with complex elements, the parameter list may contain a set of names that map
to the structure of the list elements:

    val mixed = [ ["int", 1], ["alpha", "a"], ["int", 2] ];
    [ mixed as ([val type, val integer]) -> fibonacci(integer) ];

Since it is pattern matching, it may also be used for filtering:

    [ mixed as (["int", val integer]) -> fibonacci(integer) ];

The operation can be any valid code block, including the definition and use
of local functions to the simplest return:

    [ mixed as (["int", val integer]) -> func f(x) { x * 2 }; f(integer); ];
    [ mixed as (["int", val integer]) -> integer ];

= String Comprehensions

TODO

= Binary Comprehensions

TODO
