= Process Management

funq encourages the developer to implement applications as set of highly atomic
processes. Each network connection, each source of file I/O, each computational
unit, etc. ought to be run in its own process. While it is possible to manage
the set of processes manually by calling std.process::exec(func), there is a
more powerful approach available: supervisors.

Supervisors are processes whose job it is to start, stop, and when necessary
restart processes under their control. This allows one to create robust
applications where processes may come and go, and even crash on error,
while maintaining stable, predictable behavior.

Supervisors are typically arranged in a tree with one "top" supervisor watching
other supervisors which have specific jobs to do. While it is possible to create
just one supervisor that looks after all of an application's process needs, it
is typically more manageable to split up the responsibilities between several
supervisors.

= Defining A Supervision Tree

funq provides a declarative syntax for defining supervision tree this is nearly
identical to Qt's QML language, with the major exception of not supporting
Javascript blocks, and reminiscent of the popular JSON format. Files containing
a supervision tree definition should have the "fnqs" suffix and be placed in
the "contents/supervision" directory in the application package. (See the
documentation on packaging for more on the layout of applications packages.)

The funq compiler toolchain processes the declarative contents of fnqs files
into a private data structure that can be accessed via the standard library. A
funq package may contain multiple supervision tree definitions, though often one
will suffice.

Defining a new supervisor in a fqns file is as easy as:

    Supervisor {
        id: mySupervisor
    }

This supervisor can be accessed using the supervisor standard library:

    val supervision = std.supervisor::create("mySupervisor");

Calling create more than once with the same supervisor tree ID allows one to
make multiple instances of the same tree structure. To reference these later,
either hold on to the returned supervision value or give the supervisor a name:

    val supervision = std.supervisor::create("mySupervisor", "First");
    val supervision2 = std.supervisor::create("mySupervisor", "Second");
    supervision = std.supervisor::find("First"); // works
    supervision = std.supervisor::find("Second"); // error!
    supervision2 = std.supervisor::find("Second"); // works
    std.supervisor::find("Third"); // error!

Supervisors may be nested to create a "tree" of supervision:

    Supervisor {
        id: mySupervisor

        Supervisor {
            id: innerSupervisor
        }

        Supervisor {
            id: anotherOne

            Supervisor {
                id: deeplyNested
            }
        }
    }

Regardless of nesting or number of supervisors, each fnqs file must have exactly
one top-level supervisor and within a funq package each top-level supervisor
must have a unique name so it may be uniquely addressed. Supervision trees
are only visible to the current funq package. (See the documentation on packaging
for further information.)

Accessing a nested supervisor is a matter accessing the appropriate field in
the supervisor data structure:

    val supervision = std.supervisor"::create("mySupervisor");
    supervision.anotherOne.deeplyNested;

= Starting Processes

Process definitions are added to supervisors by simply including a process
block in a supervisor block:

    Supervisor {
        id: mySupervisor

        Process {
            id: myProcess
            func: org.myproject.process
        }
    }

This defines that a single process named myProcess will be started. There is no
limit to the number of process blocks may appear in a supervisor block. Process
blocks may only appear in supervisor blocks.

To increase the number of instances of this process will be created use the
poolSize property:

    Supervisor {
        id: mySupervisor

        Process {
            id: myProcess
            func: org.myproject.process
            poolSize:10
        }
    }

Now myProcess will spawn 10 copies of itself when the supervision tree is
instantiated.

A supervisor may start a process in one of two ways:

    * automatically when the supervisor starts, aka "static"
    * spawning

The above example defines a static process; when mySupervisor is created it will
exec the function org.myproject.process in a new process. This new process can
be referenced by the id "myProcess".

To make it possible to spawn the process on demand rather than on supervision
creation, define a spawn property:

    Supervisor {
        id: mySupervisor

        Process {
            id: myProcess
            func: org.myproject.process
            spawn: true
        }
    }

This defines a process known as "myProcess" which will run the function
org.myproject.process. By default there are no limits to the number of processes
may be spawned, but this can be altered by combining it with the poolSize property:

    Supervisor {
        id: mySupervisor

        Process {
            id: myProcess
            func: org.myproject.process
            spawn: true
            poolSize: 20
        }
    }

This defines the maximum number of "myProcess" processes to be 20. If more are
requested an error will be generated.

To spawn a process:

    val supervision = std.supervisor::create("mySupervisor");
    val PID = std.supervisor::spawn(supervision.mySupervisor.myProcess);

Note that when PID is returned, the new process may not have completed running
it's main function.

As with std.process::exec, a PID may be passed as the second parameter to
std.supervisor::spawn to connect to exit signals (see "tracking when a process"
exits in the functions documentation for more information):

    val supervision = std.supervisor::create("mySupervisor");
    val PID = std.supervisor::spawn(supervision.mySupervisor.myProcess,
                                    std.process::pid());

It is also possible to connect to a process's termination signals while it is
running:

    std.process::listenForTermination(sourcepID, std.process::pid());

=  Termination Policies

When a process halts, either due to error or to a clean stop, its supervisor may
take actions. These actions are defined in policy blocks like this one:

    Policy {
        id: restarter
        restart: yes
        maxFails: 5
        timeout: 5000
        log: true
    }

As with other types, policies have an id. They also define what to do when the
process halts. In the above case the policy is to restart the process a maximum
number of five times in a row with failure (successful launch resets the
counter), to wait a maximum of five seconds for the process to respond and to
log the event.

Recognized values for restart are:

    * no: the default, and means that the process will not be replaced
    * yes: a process that crashes will be restarted
    * all: if one process crashes, all processes covered by the policy will be
           restarted

The maxFails property must be a positive integer and if not specified defaults
to unlimited failures. The timeout property is in milliseconds and if not
specified it defaults to infinity.

Policies can be associated with one of two properties in supervisor and
process blocks: onFailure and onCleanExit. The onFailure policy is used when the
process terminates abnormally (crashes) with an error, while onCleanExit is used
whenever a process exits normally. One, both or none of these properties may be
set.

Setting the termination policies in a supervisor block results in them being
applied to all processes under the supervisor's control. Setting the policies
in a process block has them apply only to that process, and this overrides
whatever policies may be defined in the supervisor block it is in.

= Process isolation

TODO

Use cases:

----------
VM 1 is running application X.
VM 2 is running application Y.

Application Y has a "border control zone" (BCZ) where it is accepting
"ambassador" processes.

Application X sends an ambassador process from VM 1 to VM 2 where it passes
through Application Y's BCZ and put into a sandbox with minimal access to API
and resources.

From the user's POV:

They get into the car with their phone. The phone announces itself to the car.
The phone rings and the car automatically mutes the music that is playing.

During this entire exchange the phone's ambassador process is limited by VM2
and ultimately managed by Application Y which can kill it, revoke access from
VM1, etc.
----------
VM 1 is running a application such as Live or Cubase, usually modern usage contains
several software based synths running on same machine, and is resource hungry

VM 2 is running a visual effects software that need be in sync with the music app on
VM1, but is another resource monster eater

VM 3 is running a proper distribution channels for all the video and audio directions,
like you have a museum multimedia installation where all parts are interconnected in
different rooms

It can spans in case of live presentations to have mixed live video with recorded video
and video effects.

For the current world, it relies in external solutions, like several computers, all running
independent software, using an specific control method, that, when is not proprietary,
it relies on MIDI time code.

So, unless you have a well designed several multicore computer ( and backup ) with
applications designed to work each other ( not likely ), only complex solutions to
control and supervise this several machines is needed.

The idea behind the process owner would help in plug processes that could not affect
the master controller to mess with other resources
----------


= Summary of the supervision syntax

fqns files have one line per statement, and newline ends a statement. Properties
follow the syntax pattern:

    propertyName: value

and groups of properties are combined into blocks delimited by curly braces to
create objects:

    Supervisor {
        id: mySupervisor
        onFailure: restarter
    }

All objects require an id property which makes the object addressable by
functions. The order of object and property declaration does not matter, and
there are no order-of-execution guarantees.

The types of objects are:
    * Supervisor
    * Process
    * Policy
