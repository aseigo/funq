= Module names

'std.' is a reserved module name prefix for built in functions

= Variables

* bool
* int
* int64
* string
* float
* doubl
* true: boolean
* false: boolean

= Function Tags

* precondition: usable in the precondition clause for a function
                enforces a truth-value return
* test: a test block
* public: a function that is visible outside of its Module
* receiver: a function that receives messages from the message queue
            enforces three parameters and allows the function to be
            introspected for adding to a process' message handlers
* main: a function to be used in a process with a message queue
* statefull: a function to be used in a process with state carried
* worker: a function to be used in a processes without a message queue
