/*
 * Copyright (C) 2014 Aaron Seigo <aseigo@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 */

#include "funqprojectpackage.h"

#include <QDir>

FunqProjectPackage::FunqProjectPackage(QObject *, const QVariantList &)
{
}

void FunqProjectPackage::initPackage(KPackage::Package *package)
{
    package->setDefaultPackageRoot(QDir().canonicalPath());
    package->setContentsPrefixPaths(QStringList());

    package->addDirectoryDefinition("build", "build", tr("Build artifacts"));
    package->addDirectoryDefinition("cache", "cache", tr("Prepopulated cache files"));
    package->addDirectoryDefinition("config", "config", tr("Configuration files"));
    package->addDirectoryDefinition("data", "data", tr("Data files"));
    package->addDirectoryDefinition("distribution", "distribution", tr("Distributable packages"));
    package->addDirectoryDefinition("icons", "icons", tr("Icons"));
    package->addDirectoryDefinition("native", "native", tr("Additonal C/C++ code"));
    package->addDirectoryDefinition("resources", "resources", tr("Resource files"));
    package->addDirectoryDefinition("source", "source", tr("funq source code"));
    package->addDirectoryDefinition("supervision", "supervision", tr("Supervision definitions"));

    package->addFileDefinition("metadata", "metadata", tr("Metadata"));
    package->addFileDefinition("dependencies", "dependencies", tr("Dependencies"));
}

void FunqProjectPackage::pathChanged(KPackage::Package *package)
{
    Q_UNUSED(package)
    //TODO, if needed
}

K_EXPORT_PACKAGE_PACKAGE_WITH_JSON(FunqProjectPackage, "plasma-packagestructure-funqproject.json")

#include "funqprojectpackage.moc"

