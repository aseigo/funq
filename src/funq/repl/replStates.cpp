/*
 * Copyright (C) 2014 Aaron Seigo <aseigo@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 */

#include "replStates.h"

#include <unistd.h>
#include <iostream>

#include <readline/readline.h>
#include <readline/history.h>

#include <QDebug>
#include <QEvent>
#include <QStateMachine>

ReadState::ReadState(QState *parent)
    : QState(parent)
{
}

void ReadState::onEntry(QEvent *event)
{
    Q_UNUSED(event)
    char *line = readline(prompt());

    if (!line) {
        std::cout << std::endl;
        emit exitRequested();
        return;
    }

    // we have actual data, so let's wait for a full line of text
    QByteArray input(line);
    const QString text = QString(line).simplified();
    //qDebug() << text;

    if (text.length() > 0) {
        add_history(line);
    }

    free(line);
    emit command(text);
}

const char *ReadState::prompt() const
{
    return "> ";
}

UnfinishedReadState::UnfinishedReadState(QState *parent)
    : ReadState(parent)
{
}

const char *UnfinishedReadState::prompt() const
{
    return "  ";
}

EvalState::EvalState(QState *parent)
    : QState(parent),
      m_complete(false)
{
}

void EvalState::onEntry(QEvent *event)
{
    QStateMachine::SignalEvent *e = dynamic_cast<QStateMachine::SignalEvent*>(event);

    if (!e || e->arguments().isEmpty()) {
        if (m_complete) {
            emit completed();
        } else {
            emit continueInput();
        }
        return;
    }

    const QString command = e->arguments()[0].toString();

    if (!command.isEmpty()) {
        //TODO: the command needs to be sent to the vm
        //TODO: if the compiler says we do not have a complete
        //      then continueInput()
        //TODO: this needs actual feedback from the vm for whether or not the
        //      command is "complete" or not
        m_complete = command.right(1) == ";";
        emit output("Processing ... " + command.right(1));
    }
}

PrintState::PrintState(QState *parent)
    : QState(parent)
{
}

void PrintState::onEntry(QEvent *event)
{
    QStateMachine::SignalEvent *e = dynamic_cast<QStateMachine::SignalEvent*>(event);

    if (e && !e->arguments().isEmpty()) {
        const QString command = e->arguments()[0].toString();
        QTextStream stream(stdout);
        stream << command << "\n";
    }

    emit completed();
}

#include "moc_replStates.cpp"
